Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

    The logger.log is different because the logger.properties defines it to be that way. The line: java.util.logging.ConsoleHandler.level = INFO
    Sets the console to only show things that are of level info, while the FileHandler is set to ‘ALL’.

2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

    The message comes from the ConditionEvaluator.

3.  What does Assertions.assertThrows do?

    It is a way of saying this should throw an exception. If an exception is not thrown, or if it is of a different type, this method will fail.

4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    
    Java serialVersionUID is a version number associated with each serializable class. When deserializing Java uses this number to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible. This is important as it makes sure information stays.
    
    2.  Why do we need to override constructors?
        
    Because constructors are not inherited. This is because constructors are special as they share their name with the class name.
    
    3.  Why we did not override other Exception methods?
    
    Because those were the only ones that we need to change anything for. If we had needed other methods to do things differently then we could have.
    	
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

    It attempts to open the logging configuration file, if the file is not opened it then prints a warning “Logging not configured (console output only)”.
    
6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

    BitBucket uses markdown, the README.md file is a file that is part of that markdown. The .md extension says that the file is written in markdown.
    
7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

    The test is failing because it threw an exception that was not expected. It was also throwing a NullPointerException. We need to adjust the code so that the null pointer is not thrown also.
    
8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

    The issue is that it would throw a NullPointerException after throwing the TimerException. The NullPointerException was unexpected and therefor would fail. I fixed this by moving the check outside the try catch block so that the finally would not happen if it failed and a TimerException was thrown.
    
9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
    ![alt text](Screenshots/JUnitTest.png "JUnit Image")

10.  Make a printScreen of your eclipse Maven test run, with console
    ![alt text](Screenshots/Maven.png "Maven Image")

11.  What category of Exceptions is TimerException and what is NullPointerException

     TimerException is a checked exception, while NullPointerException is an unchecked exception.
    
12.  Push the updated/fixed source code to your own repository.

     [Link to repository](https://AshleyLuEllen@bitbucket.org/ashleylu_couch/exceptionrunner.git)